import java.util.*;
 
/** Quaternions. Basic operations. */
public class Quaternion {

   // TODO!!! Your fields here!

   /** Constructor from four double values.
    * @param a  real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
	private double r=0,i=0,j=0,k=0;
	private double vordlus=0.000000001;
	
   public Quaternion (double a, double b, double c, double d) {
      r=a;i=b;j=c;k=d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return r; // TODO!!!
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return i; // TODO!!!
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return j; // TODO!!!
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return k; // TODO!!!
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
	   String word=getRpart()+"+"+getIpart()+"i+"+getJpart()+"j+"+getKpart()+"k";
      return word; // TODO!!!
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
		s = s.replaceAll("i", "");
		s = s.replaceAll("j", "");
		s = s.replaceAll("k", "");
		String elemendid[] = s.split("\\+");

		try {
		Quaternion quater = new Quaternion(
				Double.parseDouble(elemendid[0]),
				Double.parseDouble(elemendid[1]), 
				Double.parseDouble(elemendid[2]),
				Double.parseDouble(elemendid[3]));
		return quater;

		} catch (Exception error) {
		throw new IllegalArgumentException("Incorrect input format:" + s
		+ ". Correct format is: a+bi+cj+dk");}

	}

   
   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
		  try{
			  Quaternion clone=new Quaternion(r,i,j,k);
		      return clone; // TODO!!!
		  } catch(Exception error){
			  throw new CloneNotSupportedException("Input data is incorrect: "+toString());
		  }
	   }
	   

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
	  if(((getRpart() >= -vordlus)) &&  ((getRpart() <= vordlus)) && ((getIpart() >= -vordlus)) &&  ((getIpart() <= vordlus)) 
			  && ((getJpart() >= -vordlus)) &&  ((getJpart() <= vordlus)) && ((getKpart() >= -vordlus)) &&  ((getKpart() <= vordlus))){
		  return true;
	  }
      return false; // TODO!!!
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
	  Quaternion vastus = new Quaternion(getRpart(), getIpart()*-1, getJpart()*-1, getKpart()*-1);
      return vastus; // TODO!!! 
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
	   Quaternion vastus = new Quaternion(getRpart()*-1, getIpart()*-1, getJpart()*-1, getKpart()*-1);
	      return vastus; // TODO!!! 
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
	   Quaternion vastus=new Quaternion(getRpart()+q.getRpart(), getIpart()+q.getIpart(), getJpart()+q.getJpart(), getKpart()+q.getKpart());
      return vastus; // TODO!!!
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
	  double a1=getRpart(),b1=getIpart(),c1=getJpart(),d1=getKpart();
	  double a2=q.getRpart(),b2=q.getIpart(),c2=q.getJpart(),d2=q.getKpart();
	  Quaternion vastus=new Quaternion((a1*a2-b1*b2-c1*c2-d1*d2), (a1*b2+b1*a2+c1*d2-d1*c2),
			  (a1*c2-b1*d2+c1*a2+d1*b2), (a1*d2+b1*c2-c1*b2+d1*a2));
      return vastus; // TODO!!!
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
	   Quaternion vastus = new Quaternion(getRpart()*r, getIpart()*r, getJpart()*r, getKpart()*r);
	      return vastus; // TODO!!! 
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
	   		if(isZero()==false){
			double a=getRpart(),b=getIpart(),c=getJpart(),d=getKpart();
			Quaternion vastus = new Quaternion(a/(a*a+b*b+c*c+d*d),((-b)/(a*a+b*b+c*c+d*d)),
					((-c)/(a*a+b*b+c*c+d*d)), ((-d)/(a*a+b*b+c*c+d*d)));
			return vastus;}
	   		else{
	   			throw new RuntimeException("Vale sisend: "+ toString()+". Ei saa jagada nulliga.");
	   		}
		
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
	   Quaternion vastus=new Quaternion(getRpart()-q.getRpart(), getIpart()-q.getIpart(), getJpart()-q.getJpart(), getKpart()-q.getKpart());
	      return vastus; // TODO!!! 
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
	  q.inverse();
	  Quaternion vastus=this.times(q.inverse());
      return vastus; // TODO!!!
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
	   	q.inverse();
		  Quaternion vastus=q.inverse().times(this);
	      return vastus; // TODO!!!
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
	   double a1=getRpart(),b1=getIpart(),c1=getJpart(),d1=getKpart();
	   double a2=((Quaternion) qo).getRpart(),b2=((Quaternion) qo).getIpart(),c2=((Quaternion) qo).getJpart(),d2=((Quaternion) qo).getKpart();
	   double requals=a1-a2,iequals=b1-b2,jequals=c1-c2,kequals=d1-d2;
	   if(((requals >= -vordlus)) &&  ((requals<= vordlus)) && ((iequals >= -vordlus)) &&  ((iequals <= vordlus)) 
				  && ((jequals >= -vordlus)) &&  ((jequals <= vordlus)) && ((kequals >= -vordlus)) &&  ((kequals <= vordlus))){
		   return true;
	   }
	
      return false; // TODO!!!
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
	   Quaternion vastus=this.times(q.conjugate()).plus(q.times(conjugate()));
	   vastus=vastus.times(0.5);
      return vastus; // TODO!!!
   
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return this.toString().hashCode(); // TODO!!!
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
	   double a=getRpart(),b=getIpart(),c=getJpart(),d=getKpart();
	   return Math.sqrt(a*a+b*b+c*c+d*d);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
	   Quaternion arv1 = new Quaternion(0, 0, 0., 0.);
	   Quaternion arv2 = new Quaternion(0, 0, 0., 0.);
	  
		System.out.println(arv1.equals(arv2));
		
   }
}
// end of file
